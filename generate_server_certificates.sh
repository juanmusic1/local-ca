#! /bin/bash

#openssl req -x509 -days 3650 -nodes -key pki/private/ca.key -outform der -out CA.der -extensions v3_ca

openssl genrsa -out test-ca.key 2048
openssl req -new -sha256 -key test-ca.key -subj "/C=CO/ST=AN/L=Medellin/O=Nitales S.A.S/OU=Community/CN=test-ca.tk" -out test-ca.csr
#openssl req -new -sha256 -key pki/private/ca.key -subj "/C=CO/ST=AN/O=JuanMusic1, Inc./CN=test-ca.tk" -out test-ca.csr
#openssl req -new -key pki/private/ca.key -out test-ca.csr

# Verify the content
openssl req -in test-ca.csr -noout -text -subject

./easyrsa import-req test-ca.csr test-ca

./easyrsa sign-req server test-ca

openssl x509 -req -in test-ca.csr -CA pki/ca.crt -CAkey pki/private/ca.key -CAcreateserial -out test-ca.crt -days 500 -sha256

# Verify the content
openssl x509 -in test-ca.crt -text -noout
