#! /bin/bash

# Init the easy-rsa
./easyrsa init-pki

# Config the vars
nano vars

# Build the C.A certificate
./easyrsa build-ca
#./easy-rsa build-ca nopass

# Install into ubuntu
#sudo cp ca.crt /usr/local/share/ca-certificates/
#sudo update-ca-certificates
